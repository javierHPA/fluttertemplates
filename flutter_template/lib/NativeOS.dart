import 'package:flutter/material.dart';
import 'dart:ui';

class NativeOS{
  NativeOS._();
  static String twoDigits(int n) {
    if (n >= 10) return "$n";
    return "0$n";
  }
  static int adj(){

    double height = window.physicalSize.height/window.devicePixelRatio;
    //print("height Native: $height");
    //print("pixratio: ${window.devicePixelRatio}");


    if(height<570)
      return 0;
    else if(height >=570 && height<600)
      return 1;
    else if (height>=600 && height<730)
      return 2;
    else if (height>=730 && height<800)
      return 3;
    else if(height>=800 && height<880)
      return 4;
    else if(height>=880)
      return 5;
    else
      return 2;
  }
}