
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_template/Component/EmailTextForm.dart';

import 'Component/PasswordTextForm.dart';
import 'NativeOS.dart';

// ignore: must_be_immutable
class ForgetPasswordScreen extends StatefulWidget {

  MediaQueryData mediaQuery;
  double padding;
  String image;
  bool isCenter;
  String title;
  String subTitle;
  TextEditingController textEmailController;
  Color emailBorderColor;
  Color emailBackgroundColor;
  Color emailTextColor;
  String hintText;
  VoidCallback onPressed;
  double buttonCorners;
  String textButton;
  Color textColorButton;
  Color colorButton;
  Color buttonBorder;
  Color backBtnColor;
  FocusNode emailFcsNode;
  bool isEmailError = false;
  final Function (String value) emailValidator;

  ForgetPasswordScreen(this.mediaQuery, {
    this.backBtnColor = Colors.black,
    this.padding = 32,
    this.image = '',
    this.isCenter = false,
    this.title = '',
    this.subTitle = '',
    this.emailBorderColor = Colors.amber,
    this.emailBackgroundColor = Colors.blueAccent,
    this.emailTextColor = Colors.black,
    this.emailFcsNode,
    this.hintText = '',
    this.buttonCorners = 8,
    this.buttonBorder = const Color(0xFF000000),
    this.colorButton = Colors.black,
    this.textColorButton = Colors.black,
    this.textButton = '',
    this.emailValidator,
    this.isEmailError  = false,
    @required this.onPressed,
    @required this.textEmailController
  }) : assert(padding <= 32),
        assert(mediaQuery != null),
        assert(image.isNotEmpty),
        assert(textEmailController != null),
        assert(onPressed != null);
  @override
  State<StatefulWidget> createState() => ForgetPasswordScreenState();
}

class ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  FocusNode _emailFocusNode = FocusNode();

  InputDecoration emailDeco;
  TextInputAction actNext = TextInputAction.next;
  TextInputAction actDone = TextInputAction.done;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    emailDeco = InputDecoration(
      filled: true,
      fillColor: widget.emailBackgroundColor,
      contentPadding: EdgeInsets.all(8),
      labelText: widget.hintText,
      labelStyle: TextStyle(color: (widget.emailFcsNode.hasFocus ? widget.emailTextColor: (widget.isEmailError ? Colors.red.shade700 : widget.emailTextColor ))),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.emailBorderColor, width: 1)
      ),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.emailBorderColor, width: 1)
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.emailBorderColor, width: 1)
      ),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.emailBorderColor, width: 1)
      ),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.emailBorderColor, width: 1)
      ),
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _emailFocusNode.dispose();
  }
  @override
  Widget build(BuildContext context){ return

    Stack(children: [
      Scaffold(
          resizeToAvoidBottomPadding: false,
          resizeToAvoidBottomInset: false,
          body:
          GestureDetector(onTap: (){
            SystemChannels.textInput.invokeMethod('TextInput.hide');
            FocusScope.of(context).requestFocus(FocusNode());},child:
          SafeArea(child:
          Stack(children: <Widget>[
            OrientationBuilder(builder: (ctx, orientation) =>
                Stack(children: [
                  Container(
                    width: widget.mediaQuery.size.width,
                    height: widget.mediaQuery.size.height,
                    padding: EdgeInsets.symmetric(horizontal: (orientation == Orientation.portrait ? widget.padding : widget.padding * 2)),
                    child: Flex(direction: Axis.vertical,
                      children: <Widget>[
                        Container(
                          alignment: (widget.isCenter ? Alignment.center : Alignment.centerLeft),
                          margin: EdgeInsets.only(top: orientation == Orientation.portrait ?(widget.mediaQuery.size.height * .05) : (widget.mediaQuery.size.height * .045), bottom: (orientation == Orientation.portrait ? (widget.mediaQuery.size.height * .04) : (widget.mediaQuery.size.height * .045))),
                          child: Image.asset(widget.image, width: (orientation == Orientation.portrait ? 100 : 70), filterQuality: FilterQuality.high,),
                        ),
                        Container(
                          //margin: EdgeInsets.only(bottom: (orientation == Orientation.portrait ? (widget.mediaQuery.size.height * .36) : (widget.mediaQuery.size.height * .08))),
                          child: Form(
                            autovalidate: true,
                            child: Flex(direction: Axis.vertical,
                              children: <Widget>[
                                Container(
                                  alignment: (widget.isCenter ? Alignment.center : Alignment.centerLeft),
                                  margin: EdgeInsets.only(bottom:orientation == Orientation.portrait ? 32:10),
                                  child: Text(widget.title, style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold),),
                                ),
                                Container(
                                  alignment: (widget.isCenter ? Alignment.center : Alignment.centerLeft),
                                  margin: EdgeInsets.only(//left: 16, right: 16,
                                      bottom: orientation == Orientation.portrait ?(widget.mediaQuery.size.height * .05) : (widget.mediaQuery.size.height * .045)),
                                  child: Text(widget.subTitle, style: TextStyle(fontSize: 17),),
                                ),
                                MTextForm(
                                  type: 1,
                                  customDecoration: emailDeco,
                                  actionNext: actNext,
                                  isError: widget.isEmailError,
                                  validator: widget.emailValidator,
                                  focusNode: _emailFocusNode,
                                  nextTextForm: FocusNode(),
                                  textEditingController: widget.textEmailController,
                                  hintText: widget.hintText,
                                  colorBorder: widget.emailBorderColor,
                                  textColor: widget.emailTextColor,
                                  backgroundColor: widget.emailBackgroundColor,
                                )
                                /*
                            EmailTextForm(
                              focusNode: _emailFocusNode,
                              nextTextForm: FocusNode(),
                              textEditingController: widget.textEmailController,
                              hintText: widget.hintText,
                              colorBorder: widget.emailBorderColor,
                              textColor: widget.emailTextColor,
                              backgroundColor: widget.emailBackgroundColor,
                            )
                            */
                              ],
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),


                ],)

            ),
            Positioned(top: 0,child:
            Container(alignment: Alignment.centerLeft,height: 35,width: MediaQuery.of(context).size.width,child:IconButton(icon: Icon(Platform.isIOS?Icons.arrow_back_ios:Icons.arrow_back, color: widget.backBtnColor ,),onPressed:()=>
                Navigator.pop(context)
              ,))),

          ],)
          ),)
      ),

    Positioned(bottom:20,right: 26,left: 26,child:
  OrientationBuilder(builder: (ctx, orientation){
      return
      Container(
        width: widget.mediaQuery.size.width,
        margin: EdgeInsets.only(top: (orientation == Orientation.portrait ? (widget.mediaQuery.size.height * .22) : (widget.mediaQuery.size.height * .045))),
        child:   SizedBox(
          width: (orientation == Orientation.portrait ? (widget.mediaQuery.size.width * .829) : (widget.mediaQuery.size.width * .82)),
          height: 40,
          child: RaisedButton(onPressed: widget.onPressed,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(widget.buttonCorners), side: BorderSide(color: widget.buttonBorder)),
            color: widget.colorButton,
            child: Text(widget.textButton, style: TextStyle(color: widget.textColorButton),),
          ),
        ),

      );
    }),)

    ],);

  }
}


class RegisterScreen extends StatefulWidget{

  MediaQueryData mediaQueryData;



  double ttlTxtSize = 14;
  double subttlTxtSize = 12;
  double termsTxtSize = 10;

  String title = "Registro";

  final Function (String value) nameValidator;
  final Function (String value) emailValidator;
  final Function (String value) passValidator;
  final Function (String value) pass2Validator;


  String companyNameHntTxt = "Nombre o Empresa *";
  String emailHntTxt = "E-mail*";
  String passHntTxt = "Contraseña";
  String pass2HntTxt = "Confirmar contraseña";

  bool isCenter = false;

  String btnTxt = "Registro";

  Color mainTxtColor = Colors.black;
  Color btnTxtColor = Colors.white;

  double mainMarginVertical = 10;
  double mainMarginHorizontal = 20;
  VoidCallback onPressed;


  TextEditingController nameTxtEdtgCtrllr;
  TextEditingController emailTxtEdtgCtrllr;
  TextEditingController passTxtEdtgCtrllr;
  TextEditingController pass2TxtEdtgCtrllr;

  FocusNode nameFcsNode;
  FocusNode emailFcsNode;
  FocusNode passFcsNode;
  FocusNode pass2FcsNode;

  double padding = 32;
  double btnBorderSize = 10;

  Color backBtnColor;
  Color textFieldBorderColor = Colors.purple;
  Color textFieldColor = Colors.purple.withOpacity(.4);

  Color btnBorderColor = Colors.purple;
  Color btnColor = Colors.purple;
  Color backgroundColor = Colors.white;

  String termsTxt1 = "Al registrarme, acepto los ";
  String termsTxt2 = "Términos de servicio y la Política de privacidad de wivi";

  String imgRoute  ="";

  bool isNameError = false;
  bool isEmailError = false;
  bool isPassError = false;
  bool isPass2Error = false;

  bool isPassObscure = false;
  bool isPass2Obscure = false;


  RegisterScreen({
    @required this.mediaQueryData,
    this.backBtnColor = Colors.black,
    @required this.onPressed,
    this.isNameError = false,
    this.isEmailError = false,
    this.isPassError = false,
    this.isPass2Error = false,
    this.isPassObscure = false,
    this.isPass2Obscure = false,
    this.nameValidator,
    this.emailValidator,
    this.passValidator,
    this.pass2Validator,
    this.ttlTxtSize = 14,
    this.subttlTxtSize = 12 ,
    this.termsTxtSize = 10,
    this.btnBorderSize = 10,
    this.title = "",
    this.companyNameHntTxt = "",
    this.emailHntTxt = "",
    this.passHntTxt = "Contraseña",
    this.pass2HntTxt = "Confirmar contraseña",
    this.isCenter = false,
    this.btnTxt = "",
    this.mainTxtColor = Colors.black,
    this.btnTxtColor = Colors.white,
    this.mainMarginVertical = 10,
    this.mainMarginHorizontal = 20,
    @required this.nameTxtEdtgCtrllr,
    @required this.emailTxtEdtgCtrllr,
    @required this.passTxtEdtgCtrllr,
    @required this.pass2TxtEdtgCtrllr,
    @required  this.nameFcsNode,
    @required this.emailFcsNode,
    @required this.passFcsNode,
    @required this.pass2FcsNode,
    this.padding = 32,
    this.textFieldBorderColor = Colors.black,
    this.textFieldColor = Colors.white,
    this.btnBorderColor = Colors.black,
    this.btnColor = Colors.black,
    this.backgroundColor = Colors.white,
    this.imgRoute = "",
    this.termsTxt1 = "",
    this.termsTxt2 = ""
  }):assert(mediaQueryData != null),
  assert(nameTxtEdtgCtrllr !=null), assert(emailTxtEdtgCtrllr !=null),
  assert(passTxtEdtgCtrllr != null),assert(pass2TxtEdtgCtrllr != null),
  assert(nameFcsNode !=null), assert(emailFcsNode !=null),
  assert(passFcsNode !=null), assert(pass2FcsNode != null),
  assert(onPressed !=null);





  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterScreenState();
  }

}
class RegisterScreenState extends State<RegisterScreen> {


  TextStyle boldTxtStyle;
  TextStyle normalTxtStyle;

  InputDecoration nameDeco,emailDeco, passDeco, passDeco2;
  TextInputAction actNext = TextInputAction.next;
  TextInputAction actDone = TextInputAction.done;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    boldTxtStyle =  TextStyle(color: widget.mainTxtColor,fontWeight: FontWeight.bold, fontSize: widget.ttlTxtSize);
    normalTxtStyle = TextStyle(color: widget.mainTxtColor, fontSize: widget.ttlTxtSize);


    nameDeco = InputDecoration(
      filled: true,
      fillColor: widget.textFieldColor,
      contentPadding: EdgeInsets.all(8),
      labelText: widget.companyNameHntTxt,
      labelStyle: TextStyle(color: (widget.nameFcsNode.hasFocus ? widget.mainTxtColor : (widget.isNameError ? Colors.red.shade700 : widget.mainTxtColor ))),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
    );

    emailDeco = InputDecoration(
      filled: true,
      fillColor: widget.textFieldColor,
      contentPadding: EdgeInsets.all(8),
      labelText: widget.emailHntTxt,
      labelStyle: TextStyle(color: (widget.emailFcsNode.hasFocus ? widget.mainTxtColor : (widget.isEmailError ? Colors.red.shade700 : widget.mainTxtColor ))),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
    );

    passDeco = InputDecoration(
      filled: true,
      fillColor: widget.textFieldColor,
      contentPadding: EdgeInsets.all(8),
      labelText: widget.passHntTxt,
      labelStyle: TextStyle(color: (widget.passFcsNode.hasFocus ? widget.mainTxtColor : (widget.isPassError ? Colors.red.shade700 : widget.mainTxtColor))),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      suffixIcon: IconButton(
          icon: Icon((widget.isPassObscure ? Icons.visibility : Icons.visibility_off)),
          color: widget.textFieldBorderColor,
          onPressed: () {
            if (widget.isPassObscure) setState(() => widget.isPassObscure = false);
            else setState(() => widget.isPassObscure = true);
          }),
    );

    passDeco2 = InputDecoration(
      filled: true,
      fillColor: widget.textFieldColor,
      contentPadding: EdgeInsets.all(8),
      labelText: widget.pass2HntTxt,
      labelStyle: TextStyle(color: (widget.pass2FcsNode.hasFocus ? widget.mainTxtColor : (widget.isPass2Error ? Colors.red.shade700 : widget.mainTxtColor ))),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      suffixIcon: IconButton(
          icon: Icon((widget.isPass2Obscure ? Icons.visibility : Icons.visibility_off)),
          color: widget.textFieldBorderColor,
          onPressed: () {
            if (widget.isPass2Obscure) setState(() => widget.isPass2Obscure = false);
            else setState(() => widget.isPass2Obscure = true);
          }),
    );

   // widget.isNameError = false;
    //widget.isEmailError  = false;
    //widget.isPassError = false;
    //widget.isPass2Error = false;
    //widget.isPassObscure = false;
    //widget.isPass2Obscure = false;

    //widget.nameTxtEdtgCtrllr = TextEditingController();
    //widget.emailTxtEdtgCtrllr = TextEditingController();
    //widget.passTxtEdtgCtrllr = TextEditingController();
    //widget.pass2TxtEdtgCtrllr = TextEditingController();

    //widget.nameFcsNode = FocusNode();
    //widget.emailFcsNode = FocusNode();
    //widget.passFcsNode = FocusNode();
    //widget.pass2FcsNode = FocusNode();


  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    /*
    widget.nameTxtEdtgCtrllr.dispose();
    widget.emailTxtEdtgCtrllr.dispose();
    widget.passTxtEdtgCtrllr.dispose();
    widget.pass2TxtEdtgCtrllr.dispose();

    widget.nameFcsNode.dispose();
    widget.emailFcsNode.dispose();
    widget.passFcsNode.dispose();
    widget.pass2FcsNode.dispose();
*/
  }

  @override
  Widget build(BuildContext context) {

    var termsTxt3 = new RichText(textAlign: TextAlign.center,
      text: new TextSpan(

        // Note: Styles for TextSpans must be explicitly defined.
        // Child text spans will inherit styles from parent
        style: new TextStyle(
          fontSize: widget.termsTxtSize,
          color: widget.mainTxtColor,
        ),
        children: <TextSpan>[
          TextSpan(text:  widget.termsTxt1, style: normalTxtStyle),
          TextSpan(text: widget.termsTxt2, style: boldTxtStyle),
        ],
      ),
    );
    // TODO: implement build
    return OrientationBuilder(builder: (c,orientation){
      return Stack(children: [
        Scaffold(resizeToAvoidBottomPadding: false,

          resizeToAvoidBottomInset: false,body: _body(),),
        Positioned(left: 0,right: 0,bottom: orientation == Orientation.portrait ?(NativeOS.adj()<=1?0:NativeOS.adj()>=4?0:30):NativeOS.adj()>=4?0:30,child: Container(color: widget.backgroundColor,
            child:Container(
                margin: EdgeInsets.symmetric(horizontal: widget.mainMarginHorizontal,vertical: widget.mainMarginVertical),
                child:
                Column(children: [
                  //Button
                  Container(
                    //margin: EdgeInsets.only,
                    child: SizedBox(
                      width: widget.mediaQueryData.size.width,
                      height: 40,
                      child: RaisedButton(onPressed: widget.onPressed,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(widget.btnBorderSize), side: BorderSide(color: widget.btnBorderColor)),
                        color: widget.btnColor,
                        child: Text(widget.btnTxt, style: TextStyle(color: widget.btnTxtColor),),
                      ),
                    ),
                  ),
                  //Terminos y condiciones
                  Container(
                      margin: EdgeInsets.only(top: 20,bottom: 10),
                      alignment:Alignment.center,
                      width: widget.mediaQueryData.size.width,
                      child: termsTxt3),
                ],))

        ),),
      ],);
    },);


  }


  Widget _body(){
    double width = widget.mediaQueryData.size.width;
    double height = widget.mediaQueryData.size.height;



    return
GestureDetector(onTap: (){
  SystemChannels.textInput.invokeMethod('TextInput.hide');
  FocusScope.of(context).requestFocus(FocusNode());
},child:
      SafeArea(child:
      Stack(children: <Widget>[
        Container(
          width:width,
          height:height,
          child:
          OrientationBuilder(builder: (ctx, orientation) =>
              Stack(children: [
                Positioned(left: 0,right: 0,top:orientation == Orientation.portrait ?NativeOS.adj()<=1?30:0:30,child:
                Container(
                  width:width,
                  height:height/1.6,
                  margin: EdgeInsets.symmetric(horizontal: widget.mainMarginHorizontal,vertical: widget.mainMarginVertical),
                  //padding: EdgeInsets.symmetric(horizontal: (orientation == Orientation.portrait ? padding : padding * 2)),
                  child:
                  ListView(//Flex(direction: Axis.vertical,
                    children: <Widget>[
                      Container(
                        alignment: (widget.isCenter ? Alignment.center : Alignment.centerLeft),
                        margin: EdgeInsets.only(top: 20, bottom: (orientation == Orientation.portrait ? (height * .04) : (height * .045))),
                        child: Image.asset("${widget.imgRoute}", width: (orientation == Orientation.portrait ? 100 : 70), filterQuality: FilterQuality.high,),
                      ),
                      Container(
                        alignment: (widget.isCenter ? Alignment.center : Alignment.centerLeft),
                        margin: EdgeInsets.only(bottom: 32),
                        child: Text(widget.title, style: TextStyle(fontSize:widget.ttlTxtSize, color: Colors.black, fontWeight: FontWeight.bold),),
                      ),
                      //Nombre
                      Container(
                          alignment: (widget.isCenter ? Alignment.center : Alignment.centerLeft),
                          margin: EdgeInsets.only(bottom: (orientation == Orientation.portrait ? 15 : 6)),
                          width: widget.mediaQueryData.size.width,
                          child:
                          MTextForm(
                            type: 0,
                            customDecoration: nameDeco,
                            actionNext: actNext,
                            isError: widget.isNameError,
                            validator: widget.nameValidator,
                            focusNode: widget.nameFcsNode,
                            nextTextForm: widget.emailFcsNode,
                            textEditingController: widget.nameTxtEdtgCtrllr,
                            hintText: widget.companyNameHntTxt,
                            colorBorder: widget.textFieldBorderColor,
                            textColor: widget.mainTxtColor,
                            backgroundColor: widget.textFieldColor,
                          )),
                      //Email
                      Container(
                          alignment: (widget.isCenter ? Alignment.center : Alignment.centerLeft),
                          margin: EdgeInsets.only(bottom: (orientation == Orientation.portrait ? 15 : 6)),
                          width: widget.mediaQueryData.size.width,
                          child:
                          MTextForm(
                            type: 1,
                            customDecoration: emailDeco,
                            actionNext: actNext,
                            isError: widget.isEmailError,
                            validator: widget.emailValidator,
                            focusNode: widget.emailFcsNode,
                            nextTextForm: widget.passFcsNode,
                            textEditingController: widget.emailTxtEdtgCtrllr,
                            hintText: widget.emailHntTxt,
                            colorBorder: widget.textFieldBorderColor,
                            textColor: widget.mainTxtColor,
                            backgroundColor: widget.textFieldColor,
                          )),
                      //Password
                      Container(
                          alignment: (widget.isCenter ? Alignment.center : Alignment.centerLeft),
                          margin: EdgeInsets.only(bottom: (orientation == Orientation.portrait ? 15 : 6)),
                          width: widget.mediaQueryData.size.width,
                          child:
                          MTextForm(
                            type: 2,
                            isError: widget.isPassError,
                            customDecoration: passDeco,
                            actionNext: actNext,
                            isObscureText: widget.isPassObscure,
                            validator: widget.passValidator,
                            focusNode: widget.passFcsNode,
                            nextTextForm: widget.pass2FcsNode,
                            textEditingController: widget.passTxtEdtgCtrllr,
                            hintText: widget.passHntTxt,
                            colorBorder: widget.textFieldBorderColor,
                            textColor: widget.mainTxtColor,
                            backgroundColor: widget.textFieldColor,
                          )
                      ),
                      //Confirm Password
                      Container(
                          alignment: (widget.isCenter ? Alignment.center : Alignment.centerLeft),
                          margin: EdgeInsets.only(bottom: (orientation == Orientation.portrait ? 15 : 6)),
                          width: widget.mediaQueryData.size.width,
                          child:
                          MTextForm(
                            type: 2,
                            isError: widget.isPass2Error,
                            customDecoration: passDeco2,
                            actionNext: actDone,
                            isObscureText: widget.isPass2Obscure,
                            validator:  widget.pass2Validator,
                            focusNode: widget.pass2FcsNode,
                            nextTextForm: FocusNode(),
                            textEditingController: widget.pass2TxtEdtgCtrllr,
                            hintText: widget.pass2HntTxt,
                            colorBorder: widget.textFieldBorderColor,
                            textColor: widget.mainTxtColor,
                            backgroundColor: widget.textFieldColor,
                          )
                      ),

                    ],
                  ),
                )),
              ],)

          ),
        ),


        Positioned(top: 0,child:
        Container(alignment: Alignment.centerLeft,height: 35,width: MediaQuery.of(context).size.width,child:IconButton(icon: Icon(Platform.isIOS?Icons.arrow_back_ios:Icons.arrow_back, color: widget.backBtnColor ,),onPressed:()=>
            Navigator.pop(context)
          ,))),
      ],)
      ));
  }

}