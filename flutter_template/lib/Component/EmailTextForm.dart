

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class EmailTextForm extends StatefulWidget {

  TextEditingController textEditingController;
  Color colorBorder;
  Color backgroundColor;
  Color textColor;
  final Function (String value) validator;
  FocusNode focusNode;
  FocusNode nextTextForm;

  String hintText;

  EmailTextForm({
    @required this.textEditingController,
    @required this.validator,
    this.colorBorder = Colors.amberAccent,
    this.backgroundColor = Colors.grey,
    this.textColor = Colors.black,
    this.nextTextForm,
    this.hintText = '',
    @required this.focusNode
  }) : assert(textEditingController != null, focusNode != null),
        assert(validator !=null);

  @override
  State<StatefulWidget> createState() => EmailTextFormStats();


}

class EmailTextFormStats extends State<EmailTextForm> {

  bool _isError = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //widget.focusNode = FocusNode();
    //widget.textEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    //widget.focusNode.dispose();
    //widget.textEditingController.dispose();
  }

  @override
  Widget build(BuildContext context) => Container(
    child: TextFormField(
      focusNode: widget.focusNode,
      cursorColor: widget.colorBorder,
      controller: widget.textEditingController,
      onFieldSubmitted: (val) => FocusScope.of(context).requestFocus(widget.nextTextForm),
      maxLines: 1,
      keyboardType: TextInputType.emailAddress,
      textCapitalization: TextCapitalization.none,
      decoration: InputDecoration(
        filled: true,
        fillColor: widget.backgroundColor,
        contentPadding: EdgeInsets.all(8),
        labelText: widget.hintText,
        labelStyle: TextStyle(color: (widget.focusNode.hasFocus ? widget.colorBorder : (_isError ? Colors.red.shade700 : Colors.black))),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.colorBorder, width: 1)
        ),
        focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: Colors.red.shade700, width: 1)
        ),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: widget.colorBorder, width: 1)
        ),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: widget.colorBorder, width: 1)
        ),
      ),
      validator: widget.validator,
      style: TextStyle(color: widget.textColor),
    ),
  );
}

