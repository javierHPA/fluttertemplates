

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class PasswordTextForm extends StatefulWidget {

  TextEditingController textEditingController;
  Color colorBorder;
  Color backgroundColor;
  Color textColor;

  FocusNode focusNode;
  FocusNode nextTextForm;

  PasswordTextForm({@required this.textEditingController,
    this.colorBorder = Colors.amberAccent,
    this.backgroundColor = Colors.grey,
    this.textColor = Colors.black,
    this.nextTextForm,
    @required this.focusNode
  }) : assert(textEditingController != null, focusNode != null);

  @override
  State<StatefulWidget> createState() => PasswordTextFormState();
}

class PasswordTextFormState extends State<PasswordTextForm> {

  bool _isError = false;
  bool _isObscureText = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.textEditingController = TextEditingController();
    widget.focusNode = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    widget.textEditingController.dispose();
    widget.focusNode.dispose();
  }

  @override
  Widget build(BuildContext context) => Container(
    child: TextFormField(
      focusNode: widget.focusNode,
      cursorColor: widget.colorBorder,
      controller: widget.textEditingController,
      onFieldSubmitted: (val) => FocusScope.of(context).requestFocus(widget.nextTextForm),
      maxLines: 1,
      keyboardType: TextInputType.emailAddress,
      textCapitalization: TextCapitalization.none,
      obscureText: _isObscureText,
      decoration: InputDecoration(
          filled: true,
          fillColor: widget.backgroundColor,
          contentPadding: EdgeInsets.all(8),
          labelText: 'Contraseña',
          labelStyle: TextStyle(color: (widget.focusNode.hasFocus ? widget.colorBorder : (_isError ? Colors.red.shade700 : Colors.black))),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: widget.colorBorder, width: 1)
          ),
          focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: Colors.red.shade700, width: 1)
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: widget.colorBorder, width: 1)
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: widget.colorBorder, width: 1)
          ),
          suffixIcon: IconButton(
            icon: Icon((_isObscureText ? Icons.visibility : Icons.visibility_off)),
            color: widget.colorBorder,
            onPressed: () {
              if (_isObscureText) setState(() => _isObscureText = false);
              else setState(() => _isObscureText = true);
            }),
      ),
      style: TextStyle(color: widget.textColor),
    ),
  );
}


class MTextForm extends StatefulWidget{


  TextEditingController textEditingController;
  String label = "Password";
  Color colorBorder  = Colors.amberAccent;
  Color backgroundColor  = Colors.grey;
  Color textColor  = Colors.black;
  int type = 0;
  bool isError = false;
  bool isObscureText = true;

  InputDecoration customDecoration;
  TextInputAction actionNext;

  final Function (String value) validator;

  FocusNode focusNode;
  FocusNode nextTextForm;

  String hintText = '' ;

  MTextForm({
    @required this.textEditingController,
    @required this.validator,
    this.actionNext = TextInputAction.next,
    this.isError = false,
    this.customDecoration,
    this.isObscureText = false,
    this.colorBorder,
    this.backgroundColor,
    this.textColor,
    this.nextTextForm,
    this.hintText,
    this.label,
    @required this.type,
    @required this.focusNode
  }) : assert(textEditingController != null, focusNode!=null),
  assert(validator !=null);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MTextFormState();
  }


}

class MTextFormState extends State<MTextForm>{
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    //widget.isObscureText = false;
    //widget.isError = false;
    //widget.textEditingController = TextEditingController();
    //widget.focusNode  = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    //widget.textEditingController.dispose();
    //widget.focusNode.dispose();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

      //Normal TextField
      return
        Form(autovalidate: true,
            child:
        TextFormField(
        //autovalidate: true,
        focusNode: widget.focusNode,
        autofocus: false,
        textInputAction: widget.actionNext,
        cursorColor: widget.textColor,
        controller: widget.textEditingController,
        onFieldSubmitted: (val) => FocusScope.of(context).requestFocus(widget.nextTextForm),
        maxLines: 1,
        obscureText: widget.isObscureText,
        keyboardType: TextInputType.emailAddress,
        //textCapitalization: TextCapitalization.none,
        decoration: widget.customDecoration!=null?widget.customDecoration:InputDecoration(
          filled: true,
          fillColor: widget.backgroundColor,
          contentPadding: EdgeInsets.all(8),
          labelText: widget.hintText,
          labelStyle: TextStyle(color: (widget.focusNode.hasFocus ? widget.colorBorder : (widget.isError ? Colors.red.shade700 : Colors.black))),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: widget.colorBorder, width: 1)
          ),
          focusColor: widget.colorBorder,
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: ( widget.colorBorder), width: 1)
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: ( widget.colorBorder), width: 1)
          ),
        ),
        validator: widget.validator,
        style: TextStyle(color: widget.textColor),
      ));

  }

}