//library fluttertemplate;

import 'package:flutter/material.dart';

///start Screen
//@immutable
// ignore: must_be_immutable
class StartScreen extends StatefulWidget {
  double padding = 16.0;
  String image = '';
  String buttonOneText;
  String buttonTwoText;
  Color borderButtonOne;
  Color borderButtonTwo;
  Color colorButtonOne;
  Color colorButtonTwo;
  bool isRound2;

  Color colorButtonTextOne;
  Color colorButtonTextTwo;

  VoidCallback onPressedButtonOne;
  VoidCallback onPressedButtonTwo;

  MediaQueryData mediaQuery;

  double buttonCorners;

  StartScreen(this.mediaQuery,{
    this.padding = 32,
    @required this.image,
    this.buttonOneText = 'Botón uno',
    this.buttonTwoText = 'Botón dos',
    this.borderButtonOne = const Color(0xFF000000),
    this.borderButtonTwo = const Color(0xFF000000),
    this.colorButtonOne = Colors.transparent,
    this.colorButtonTwo = Colors.transparent,
    this.colorButtonTextOne = Colors.black,
    this.colorButtonTextTwo = Colors.black,
    @required this.onPressedButtonOne,
    @required this.onPressedButtonTwo,
    this.isRound2 = false,
    this.buttonCorners = 8.0
  }) : assert(padding <= 32),
    assert(image.isNotEmpty),
    assert(onPressedButtonOne != null),
    assert(onPressedButtonTwo != null),
    assert(mediaQuery != null);

  @override
  State<StatefulWidget> createState() => StartScreenState();
}

class StartScreenState extends State<StartScreen> {
  @override
  Widget build(BuildContext context) {
    dynamic screenHeight = widget.mediaQuery.size.height;
    dynamic screenwidth = widget.mediaQuery.size.width;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      body: OrientationBuilder(builder: (context, orientation) {
        return Container(
          width: screenwidth,
          height: screenHeight,
          padding: EdgeInsets.symmetric(horizontal: (orientation == Orientation.portrait ? widget.padding : (widget.padding * 2))),
          alignment: Alignment.bottomCenter,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: (orientation == Orientation.portrait ? (screenHeight * .23) : (screenHeight * .22)), top: (orientation == Orientation.portrait ? (screenHeight * .32) : 15)),
                  child: Image.asset(
                      widget.image, width: (orientation == Orientation.portrait ? 140 : 120), alignment: Alignment.center, filterQuality: FilterQuality.high
                  ),
                ),
                Container(
                  //alignment: Alignment.bottomCenter,
                  child: SizedBox(
                    width: screenwidth,
                    height: 40,
                    child: RaisedButton(onPressed: widget.onPressedButtonOne,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(widget.buttonCorners), side: BorderSide(color: widget.borderButtonOne)),
                      color: widget.colorButtonOne,
                      child: Text(widget.buttonOneText, style: TextStyle(color: widget.colorButtonTextOne)),
                    ),
                  ),
                ),
                widget.isRound2==true?Container(
                  margin: EdgeInsets.only(top: 12),
                  //alignment: Alignment.bottomCenter,
                  child: SizedBox(
                    width: screenwidth,
                    height: 40,
                    child: RaisedButton(onPressed: widget.onPressedButtonTwo,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(widget.buttonCorners), side: BorderSide(color: widget.borderButtonTwo)),
                      color: widget.colorButtonTwo,
                      child: Text(widget.buttonTwoText, style: TextStyle(color: widget.colorButtonTextTwo),),
                    ),
                  ),
                ):Container(
                  margin: EdgeInsets.only(top: 12),
                  //alignment: Alignment.bottomCenter,
                  child: SizedBox(
                    width: screenwidth,
                    height: 40,
                    child: FlatButton(onPressed: widget.onPressedButtonTwo,
                      child: Text(widget.buttonTwoText, style: TextStyle(color: widget.colorButtonTextTwo),),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

}
