//library fluttertemplate;

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_template/Component/EmailTextForm.dart';
import 'package:flutter_template/Component/PasswordTextForm.dart';

// ignore: must_be_immutable
class LoginScreen extends StatefulWidget {

  MediaQueryData mediaQueryData;
  double padding;
  String image;
  AlignmentGeometry imageAlignment;
  String title;
  VoidCallback onPressed;
  VoidCallback onPressed2;


  final Function (String) emailValidator;
  final Function (String) passValidator;
  bool isCenter;
  TextEditingController textEmailController;
  TextEditingController textPasswordController;
  String textButton;
  Color colorTextButton;
  Color buttonBorder;
  Color colorButton;
  double buttonCorners;
  FocusNode emailFcsNode;
  FocusNode passFcsNode;

  double ttlTxtSize = 14;
  double subttlTxtSize = 12;

  String emailHntTxt = "E-mail*";
  String passHntTxt = "Contraseña";
  String fpass = "";
  bool isPassError = false;
  bool isEmailError = false;
  bool isPassObscure = false;

  Color textFieldBorderColor = Colors.purple;
  Color textFieldColor = Colors.purple.withOpacity(.4);
  Color mainTxtColor = Colors.black;


  Color colorBackgroundTextForm;
  Color colorBorderTextForm;
  Color textFormFieldColor;
  Color backBtnColor;

  String emailHintText;

  LoginScreen(this.mediaQueryData, {
    @required this.emailValidator,
    @required this.passValidator,
    this.backBtnColor =Colors.black,
    this.padding = 32.0,
    this.title = '',
    this.image = '',
    this.ttlTxtSize = 14,
    this.subttlTxtSize = 12 ,
    this.fpass = "¿Olvidaste tu contraseña?",
    this.emailHntTxt = "Correo*",
    this.passHntTxt = "Contraseña",
    this.textFieldBorderColor = Colors.black,
    this.mainTxtColor = Colors.black,
    this.textFieldColor = Colors.white,
    this.imageAlignment = Alignment.centerLeft,
    this.isCenter = false,
    this.buttonBorder = const Color(0xFF000000),
    this.colorButton = Colors.black,
    this.colorTextButton = Colors.black,
    this.textButton = '',
    this.buttonCorners = 8,
    this.colorBackgroundTextForm = Colors.amber,
    this.colorBorderTextForm = Colors.blueAccent,
    this.textFormFieldColor = Colors.black,
    this.emailHintText = '',
    this.isPassError = false,
    this.isEmailError = false,
    @required this.onPressed,
    @required this.onPressed2,
    @required this.textEmailController,
    @required this.textPasswordController,
    @required this.emailFcsNode,
    @required this.passFcsNode,
  }) : assert(padding <= 32),
        assert(onPressed != null),
        assert(onPressed2 != null),
        assert(textPasswordController != null),
        assert(textEmailController != null),
        assert(mediaQueryData != null),
        assert(emailValidator !=null);

  @override
  State<StatefulWidget> createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  FocusNode _emailFocusNode;
  FocusNode _passwordFocusNode;

  TextStyle boldTxtStyle;
  TextStyle normalTxtStyle;

  InputDecoration nameDeco,emailDeco, passDeco, passDeco2;
  TextInputAction actNext = TextInputAction.next;
  TextInputAction actDone = TextInputAction.done;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    emailDeco = InputDecoration(
      filled: true,
      fillColor: widget.textFieldColor,
      contentPadding: EdgeInsets.all(8),
      labelText: widget.emailHntTxt,
      labelStyle: TextStyle(color: (widget.emailFcsNode.hasFocus ? widget.mainTxtColor : (widget.isEmailError ? Colors.red.shade700 : widget.mainTxtColor ))),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
    );

    passDeco = InputDecoration(
      filled: true,
      fillColor: widget.textFieldColor,
      contentPadding: EdgeInsets.all(8),
      labelText: widget.passHntTxt,
      labelStyle: TextStyle(color: (widget.passFcsNode.hasFocus ? widget.mainTxtColor : (widget.isPassError ? Colors.red.shade700 : widget.mainTxtColor))),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: widget.textFieldBorderColor, width: 1)
      ),
      suffixIcon: IconButton(
          icon: Icon((widget.isPassObscure ? Icons.visibility : Icons.visibility_off)),
          color: widget.textFieldBorderColor,
          onPressed: () {
            if (widget.isPassObscure) setState(() => widget.isPassObscure = false);
            else setState(() => widget.isPassObscure = true);
          }),
    );
    /*
    _emailFocusNode = FocusNode();
    _passwordFocusNode = FocusNode();
    widget.textEmailController = TextEditingController();
    widget.textPasswordController = TextEditingController();
    */
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    /*
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    widget.textPasswordController.dispose();
    widget.textEmailController.dispose();
    */
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(children: [
      Scaffold(
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: false,
        body:SafeArea(child:
        GestureDetector(onTap:(){
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          FocusScope.of(context).requestFocus(FocusNode());
        },child:
              Stack(children: [
                Positioned(top:0,right: 0,left: 0,child:
                Container(
                    width: widget.mediaQueryData.size.width,
                    height: widget.mediaQueryData.size.height,
                    margin: EdgeInsets.only(top: 12),
                    padding: EdgeInsets.symmetric(horizontal:  widget.padding),
                    //padding: EdgeInsets.only(top:(orientation == Orientation.portrait ? widget.padding : (widget.padding * 1.5)),left: (orientation == Orientation.portrait ? widget.padding : (widget.padding * 1.5)),right: (orientation == Orientation.portrait ? widget.padding : (widget.padding * 1.5))),
                    child: Flex(direction: Axis.vertical,
                      children: <Widget>[
                        Container(
                          alignment: (widget.isCenter ? Alignment.center : Alignment.centerLeft),
                          margin: EdgeInsets.only(top:  26 , bottom: (widget.mediaQueryData.size.height *  0.07)),
                          child: Image.asset(widget.image, width: 100, filterQuality: FilterQuality.high,),
                        ),
                        Container(alignment: (widget.isCenter ? Alignment.center : Alignment.centerLeft), width:widget.mediaQueryData.size.width, margin: EdgeInsets.only(bottom: 12),
                          child: Text(widget.title,maxLines: 1, style: TextStyle(fontSize:  20 , fontWeight: FontWeight.bold),),
                        ),
                        Form(
                          child: Flex(
                            direction: Axis.vertical,
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom:10 ),
                                  child: MTextForm(
                                    type: 1,
                                    customDecoration: emailDeco,
                                    actionNext: actNext,
                                    isError: widget.isEmailError,
                                    validator: widget.emailValidator,
                                    focusNode: widget.emailFcsNode,
                                    nextTextForm: widget.passFcsNode,
                                    textEditingController: widget.textEmailController,
                                    hintText: widget.emailHntTxt,
                                    colorBorder: widget.textFieldBorderColor,
                                    textColor: widget.mainTxtColor,
                                    backgroundColor: widget.textFieldColor,
                                  )
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: (widget.mediaQueryData.size.height * 0.3 )),
                                child: MTextForm(
                                  type: 2,
                                  isError: widget.isPassError,
                                  customDecoration: passDeco,
                                  actionNext: actNext,
                                  isObscureText: widget.isPassObscure,
                                  validator: widget.passValidator,
                                  focusNode: widget.passFcsNode,
                                  nextTextForm: FocusNode(),
                                  textEditingController: widget.textPasswordController,
                                  hintText: widget.passHntTxt,
                                  colorBorder: widget.textFieldBorderColor,
                                  textColor: widget.mainTxtColor,
                                  backgroundColor: widget.textFieldColor,
                                )
                                ,
                              ),
                            ],
                          ),
                          autovalidate: true,
                        ),

                      ],
                    )
                )),
                Positioned(top: 0,child:
                Container(alignment: Alignment.centerLeft,height: 35,width: MediaQuery.of(context).size.width,child:IconButton(icon: Icon(Platform.isIOS?Icons.arrow_back_ios:Icons.arrow_back, color: widget.backBtnColor ,),onPressed:()=>
                    Navigator.pop(context)
                  ,))),

              ],)
        ,)
        )
    ),

      Positioned(bottom:20,right: 26,left: 26, child:

          Container(child:Column(children:<Widget>[
            Container(
              margin: EdgeInsets.only(left: 10,right:10,bottom:6 ),
              child: SizedBox(
                width: widget.mediaQueryData.size.width,
                height: 40,
                child: RaisedButton(onPressed: widget.onPressed,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(widget.buttonCorners), side: BorderSide(color: widget.buttonBorder)),
                  color: widget.colorButton,
                  child: Text(widget.textButton, style: TextStyle(color: widget.colorTextButton),),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10,right:10,bottom: 6 ),
              child: SizedBox(
                width: widget.mediaQueryData.size.width,
                height: 40,
                child: FlatButton(onPressed: widget.onPressed2,
                  child: Text(widget.fpass, style: TextStyle(color: widget.colorButton,fontWeight: FontWeight.bold),),
                ),
              ),
            ),

          ]))


    ),

    ],);

  }
}